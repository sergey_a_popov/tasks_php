<?php
declare(strict_types=1);

namespace Api\Services;


class NbpApiService {
	
	private $apiUrl = 'https://api.nbp.pl/api/exchangerates/tables/a/today/?format=json';
	private $cachePath = __DIR__ . '/../../cache';
	private $cacheFileName = '';
	
	public function __construct() 
	{
		$this->cacheFileName = date('dmYH');
	}
	
	/**
	 * Returns exchange rates from NBP or from cache
	 */
	public function getRates(): ?array
	{
		$result = $this->getCached ();
		if (is_array($result)) {
			return $result;
		}
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->apiUrl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60); 
		
		$output = curl_exec($ch);

		if (!$output) {
			return null;
		}
		$result = json_decode($output, true);
		if (json_last_error() === JSON_ERROR_NONE) {
			if (isset($result[0]['rates'])) {
				$this->setCached($result[0]['rates']);
				return $result[0]['rates'];
			}
		}
			
		return null;
	}
	
	/**
	 * @param array $data
	 */
	private function setCached($data)
	{
		if (!is_dir($this->cachePath)) {
			mkdir($this->cachePath, 0777);
		}
		file_put_contents($this->cachePath . '/' . $this->cacheFileName, json_encode($data));
	}
	
	
	private function getCached (): ?array
	{
		if (file_exists($this->cachePath . '/' . $this->cacheFileName)) {
			$cache = file_get_contents($this->cachePath . '/' . $this->cacheFileName);
			$result = json_decode($cache, true);
			if (json_last_error() === JSON_ERROR_NONE) {
				return $result;
			}
		}
		return null;
	}
	
}