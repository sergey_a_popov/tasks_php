<?php
declare(strict_types=1);

namespace Api\ExchangeRates;

use Api\ResponseInterface;
use Api\Services\NbpApiService;

class CalculatorController implements ResponseInterface
{
	private $httpCode = 200;
	
	private $content = [];
	
	
    /**
     * @param array $inputArray assume any input array (like $_GET)
     * @return ResponseInterface
     */
    public function calculateCurrentExchangeRateAction(array $inputArray = []): ResponseInterface
    {
		$validationResult = $this->validateInput($inputArray);
		
		if (!is_null($validationResult)) {
			$this->generateError($validationResult);
			return $this;
		}

		if ($inputArray['fromCurrency'] == $inputArray['toCurrency']) {
			$amount = 0;
			if (isset($inputArray['amount'])) {
				$amount = $inputArray['amount'];
			}
			$this->setHttpCode(200);
			$this->setContent([
				'rate' => '1.00000',
				'amount' => number_format((float)$amount, 5, '.', '')
			]);
			return $this;
			
		}
		
		$nbpApiService = new NbpApiService();
		$rates = $nbpApiService->getRates();

		if (is_null($rates)) {
			$this->generateError('Couldn\'t get rates data from NBP');
			return $this;
		}
	
		$fromRate = 0;
		$toRate = 0;
		foreach ($rates as $rate) {
			if ($rate['code'] == $inputArray['fromCurrency']) {
				$fromRate = $rate['mid'];
				continue;
			}
			if ($rate['code'] == $inputArray['toCurrency']) {
				$toRate = $rate['mid'];
				continue;
			}
		}
		
		if (!$fromRate) {
			$this->setHttpCode(500);
			$this->setContent (['error' => 'The fromCurrency ' . $inputArray['fromCurrency'] . ' was not found' ]);
			return $this;
		}
		
		if (!$toRate) {
			$this->setHttpCode(500);
			$this->setContent (['error' => 'The toCurrency ' . $inputArray['toCurrency'] . ' was not found' ]);
			return $this;
		}
		
		$rate = $fromRate / $toRate;
		
		$amount = 0;
		if (isset($inputArray['amount'])) {
			$amount = (float)$inputArray['amount'] * $rate;
		}

		$this->setHttpCode(200);
		$this->setContent([
			'rate' => number_format($rate, 5, '.', ''),
			'amount' => number_format($amount, 5, '.', '')
		]);
		
		return $this;
    }
	
	/**
     * @param array $inputArray
     * @return string|null
     */
	private function validateInput (array $inputArray = []): ?string
	{
		if (!isset($inputArray['fromCurrency'])) {
			return 'The parameter fromCurrency is required';
		}
		if (strlen($inputArray['fromCurrency']) != 3) {
			return 'The parameter fromCurrency must be a valid ISO 4217 currency code';
		}
		if (!isset($inputArray['toCurrency'])) {
			return 'The parameter toCurrency is required';
		}
		if (strlen($inputArray['toCurrency']) != 3) {
			return 'The parameter toCurrency must be a valid ISO 4217 currency code';
		}
		if (isset($inputArray['amount']) && !is_numeric($inputArray['amount'])) {
			return 'The parameter amount  must be a number';
		}
		return null;
	}
	
	/**
     * @param atring $errDescription
     */

	private function generateError(string $errDescription)
	{
		$this->setHttpCode(500);
		$this->setContent (['error' => $errDescription]);
	}
	
    /**
     * @param array $inputArray assume any input array (like $_GET)
     * @return ResponseInterface
     */
    public function getRateStatsAction(array $inputArray = []): ResponseInterface
    {
		return $this;
    }
	
	/**
	 * @param array $content
	 */
	 
	public function setContent (array $content) {
		$this->content = $content;
	}
	
	/**
     * @param bool $jsonEncoded
     * @return mixed
     */
    public function getContent(bool $jsonEncoded = true)
	{
		if ($jsonEncoded) {
			return json_encode($this->content);
		}
		return $this->content;
		
	}
	
	 /**
	 * @param int $httpCode
	 */
    private function setHttpCode($httpCode)
	{
		$this->httpCode = $httpCode;
	}

    /**
     * @return int
     */
    public function getHttpCode(): int
	{
		return $this->httpCode;
	}

}