<?php

require_once 'bootstrap.php';

/**
 * ====== PLEASE PROVIDE THE FOLLOWING: =======
 * Development Time Estimate [in hours]:    3
 * Actual Development Time [in hours]:      2.5
 * Your thoughts and comments:              nothing :)
 * ============================================
 */

/**
 * correct input EXAMPLE - it may be modified (and will be modified during task assessment)
 */
$_GET = [
    'fromCurrency' => 'UAH',
    'toCurrency' => 'GBP',
    'amount' => 313.57,
];

$controller = new \Api\ExchangeRates\CalculatorController();
$response = $controller->calculateCurrentExchangeRateAction($_GET);


// print the returned response
http_response_code($response->getHttpCode());
header('Content-type: application/json');
echo $response->getContent(true);
