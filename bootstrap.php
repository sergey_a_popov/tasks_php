<?php

error_reporting(E_ALL);
$autoloaderFile = __DIR__.'/vendor/autoload.php';
if (!file_exists($autoloaderFile)) {
    echo 'ERROR - Autoload file not present - run "composer install"';
    die();
}
require $autoloaderFile;